package vjezbe2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringManipulation {
	public String urediTekst(String s) {
		int index = 0;
		StringBuilder res = new StringBuilder();

		// index ce biti -1 ako karakter (tocka) nije pronadjen u stringu
		while (index != -1) {

			// izbrisemo razmake s pocekta i kraja stringa
			s = s.trim();

			// trazimo prvo slovo u stringu
			Pattern p = Pattern.compile("\\p{Alpha}");
			Matcher m = p.matcher(s);
			if (m.find()) {
				// ako smo pronasli slovo, prebacimo ga u uppercase
				s = s.replaceFirst("\\p{Alpha}", s.substring(m.start(), m.start() + 1).toUpperCase());
			}

			// trazimo tocku
			index = s.indexOf('.');

			// u rezultat dodamo sadrzaj stringa do tocke i iza ubacimo razmak
			res.append(s.substring(0, index + 1) + " ");

			// iz ulaznog stringa izbacimo prvu recenicu i petlja se ponavlja od sljedece
			// recenice
			s = s.substring(index + 1, s.length());
		}
		return res.toString();
	}

	public int prebrojiPodstringove(String input, String subString) {
		return prebrojiPodstringove(input, subString, false);
	}

	public int prebrojiObrnutePodstringove(String input, String subString) {
		return prebrojiPodstringove(input, reverseString(subString));
	}

	public int prebrojiPodstringove(String input, String subString, boolean caseSensitive) {
		if (subString.length() < 1)
			return 0;
		if (!caseSensitive) {
			input = input.toLowerCase();
			subString = subString.toLowerCase();
		}
		int broj = -1;
		int index = 0;
		int fromIndex = 0;
		while (index != -1) {
			broj++;
			index = input.indexOf(subString, fromIndex);
			fromIndex = index + subString.length();
		}
		return broj;
	}

	public String zamijeniPodstringove(String input, String subString, String newString, boolean caseSensitive) {
		String regex = "";
		if (!caseSensitive) {
			regex = "(?i)";
		}
		input = input.replaceAll(regex + Pattern.quote(subString), newString);
		return input;
	}

	private String reverseString(String input) {
		StringBuilder result = new StringBuilder();
		for (int i = input.length() - 1; i >= 0; i--) {
			result.append(input.charAt(i));
		}
		return result.toString();
	}
}
