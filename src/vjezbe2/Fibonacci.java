package vjezbe2;

public class Fibonacci {
	 
    //Fibonacci rekurzija
    protected int fibonacciRekurzija(int n){
        if(n == 1 || n == 2){
            return 1;
        }
        return fibonacciRekurzija(n-1) + fibonacciRekurzija(n-2);
    }
}
