package vjezbe2;

import java.util.Scanner;

// Ucitajte niz stringova, provjerite da se sastoje samo od malih slova i leksikografski
// ih uredite (tako da prva riječ u recenici pocinje velikim slovom, dodaje se razmak
// nakon interpunkcijskih znakova, a string moze imati vise recenica) i ispisite ih na
// stadardni output.

public class Leksikografija {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Unesi tekst koji ce biti uredjen: ");
			String input = scan.nextLine();
			
			// string se sastoji od malih slova
			input.toLowerCase();
			
			StringManipulation sm = new StringManipulation();
			
			// dodaj velika slova i interpunkcijske znakove
			// ako zadnja recenica ne zavrsava tockom, bit ce izbrisana
			input = sm.urediTekst(input);
			
			// ispisi string
			System.out.println(input);
		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

}
