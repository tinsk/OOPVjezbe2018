package vjezbe2;

import java.util.Scanner;

// Napišite Test program koji učitava prirodan broj n kao argument na komandnoj
// liniji i provjerava je li taj broj član Fibonaccijevog niza koristeći klasu
// Fibonacci.java. Ponuđenu klasu potrebno je urediti tako da nema main metodu.
// Tako uređenu klasu naslijedite u novoj klasi koja će imati dodatnu metodu za
// provjeru je li broj primljen u argumentu član niza

public class FibonacciTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Unesi broj: ");
			int broj = scan.nextInt();
			FibonacciProvjera provjera = new FibonacciProvjera();
			System.out.println("\nJe li broj clan Fibonaccijevog niza? " + provjera.provjeriBroj(broj));

		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

}
