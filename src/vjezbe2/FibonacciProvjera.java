package vjezbe2;

public class FibonacciProvjera extends Fibonacci {
	public boolean provjeriBroj(int broj) {
		int n = 1;
		int fibonacci = 1;
		while (fibonacci < broj) {
			fibonacci = fibonacciRekurzija(n++);
			System.out.print(fibonacci + " ");
		}
		return broj == fibonacci;
	}
}
