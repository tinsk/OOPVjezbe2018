package vjezbe2;

import java.util.Scanner;

// Implementirajte klasu koja jednom metodom u danom stringu traži zadani
// podstring i broji ukupan broj pojavljivanja takvog stringa, drugom metodom traži
// zadani podstring, ali ispisan obrnutim smjerom, trećom metodom zamijenjuje sve
// pojave traženog podstringa sa nekim novim stringom uz opciju da pretraga bude
// case sensitive (po defaultu nije).

public class PretrazivanjeStringa {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Unesi string u kojem se trazi podstring: ");
			String input = scan.nextLine();
			System.out.print("Unesi podstring: ");
			String subString = scan.nextLine();

			StringManipulation sm = new StringManipulation();

			System.out.println("Broj pojavljivanja podstringa: " + sm.prebrojiPodstringove(input, subString));

			System.out.println(
					"Broj pojavljivanja obrnutog podstringa: " + sm.prebrojiObrnutePodstringove(input, subString));

			System.out.println("Broj pojavljivanja podstringa (case sensitive): "
					+ sm.prebrojiPodstringove(input, subString, true));
			
			System.out.print("Unesi novi string: ");
			String newString = scan.nextLine();
			System.out.println("Case-sensitive (DA/NE) Default je NE: ");
			String odgovor = scan.nextLine();
			boolean caseSensitive = odgovor.toUpperCase().equals("DA") ? true : false;
			
			System.out.println("String nakon zamjene: " + sm.zamijeniPodstringove(input, subString, newString, caseSensitive));

		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

}
