package vjezbe7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Label;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

// Label that can contain an image
// Code found here: https://coderanch.com/t/338505/java/display-image-AWT-panel
public class ImageLabel extends Label {
	Image image;

	public ImageLabel() {
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		int w = getWidth();
		int h = getHeight();
		int imageWidth = image.getWidth(this);
		int imageHeight = image.getHeight(this);
		int x = (w - imageWidth) / 2;
		int y = (h - imageHeight) / 2;
		
		// Create a white background
		Graphics2D gui = (Graphics2D) g;
		Rectangle2D rectangle = new Rectangle2D.Double(x, y, 100, 150);
		gui.setPaint(Color.WHITE);
		gui.fill(rectangle);
		
		// Draw the image
		g.drawImage(image, x, y, this);
	}

	/**
	 * For the ScrollPane or other Container.
	 */
	public Dimension getPreferredSize() {
		return new Dimension(image.getWidth(this), image.getHeight(this));
	}

	public void loadImage(String fileName) throws MalformedURLException {
		java.net.URL url = new File(fileName).toURI().toURL();
		System.out.println("url: " + url);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		image = toolkit.getImage(url);
		MediaTracker tracker = new MediaTracker(this);
		tracker.addImage(image, 0);
		try {
			tracker.waitForID(0);
			this.repaint();
		} catch (InterruptedException ie) {
			System.out.println("interrupt: " + ie.getMessage());
		}
	}
}
