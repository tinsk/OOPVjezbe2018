package vjezbe7;

import java.awt.*;
import java.awt.event.*;
import java.net.MalformedURLException;

public class Duke extends Frame implements ActionListener, MouseListener {

	final static int broj_slika = 4;
	final static int pocni_od = 0;

	// u ovo polje spremamo slike Dukea
	private String[] slike_duke = new String[broj_slika];

	// razne panele
	private Panel glavna_panela, odabir_panela, prikaz_panela;

	// korisnik odabire sliku
	private Choice izbor = null;

	// u iducoj labeli se prikazuju slike

	private ImageLabel label_prikaz;
	private Label label_kvadrat;

	// konstruktor

	public Duke() throws MalformedURLException {

		// najprije instanciramo nadklasu JFrame

		super("Duke!!");

		// kreiramo panele

		odabir_panela = new Panel();
		prikaz_panela = new Panel();

		// dodajmo komponente na te dvije subpanele
		kreiraj_komponente();

		// kreirajmo glavnu panelu i dodajmo gornje subpanele

		glavna_panela = new Panel();

		glavna_panela.setLayout(new GridLayout(2, 1, 5, 5)); // zadnja dva broja oznacavaju da sa svake strane
																// komponente ima 5 pixela mjesta
		// glavna_panela.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

		glavna_panela.add(odabir_panela);
		glavna_panela.add(prikaz_panela);

		// na kraju stavimo i glavnu panelu na frame
		// slozimo komponenete
		this.add(glavna_panela, BorderLayout.CENTER);
		this.add(label_kvadrat, BorderLayout.EAST);
		this.pack();
		this.setVisible(true);

		// aplikacija zavrsava kada se njezin prozor zatvori

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
	}

	// ova metoda kreira razne graficke komponente

	private void kreiraj_komponente() throws MalformedURLException {

		// najprije slike Duke-a strpamo u polje

		slike_duke[0] = "res/images/mahanje.gif";
		slike_duke[1] = "res/images/ok.gif";
		slike_duke[2] = "res/images/pokazivanje.gif";
		slike_duke[3] = "res/images/razmisljanje.gif";

		// za prikaz slike
		label_prikaz = new ImageLabel();

		// kreiramo combobox

		String[] dukeImena = { "Mahanje", "OK", "Pokazivanje", "Razmisljanje" };

		// kreiramo combobox

		izbor = new Choice();
		izbor.add(dukeImena[0]);
		izbor.add(dukeImena[1]);
		izbor.add(dukeImena[2]);
		izbor.add(dukeImena[3]);
		izbor.select(0);

		// prikazimo sliku sa indeksom pocni_od
		label_prikaz.loadImage(slike_duke[pocni_od]);

		// dodajmo combo izbor na odabir panelu
		odabir_panela.add(izbor);

		// prikazimo
		prikaz_panela.add(label_prikaz);
		
		// dodamo kvadrat
		label_kvadrat = new Label();
		label_kvadrat.setBackground(Color.YELLOW);
		label_kvadrat.setPreferredSize(new Dimension(100,100));
		label_kvadrat.addMouseListener(this);

		// procesiramo dogadjaje na combobox-u.
		izbor.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
			    if (e.getStateChange() == ItemEvent.SELECTED) {
			        label_prikaz.setVisible(true);
			        try {
			        	System.out.println("Selected index: " + izbor.getSelectedIndex());
			        	System.out.println("Selected item: " + izbor.getSelectedItem());
						label_prikaz.loadImage(slike_duke[izbor.getSelectedIndex()]);
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    } else {
			        label_prikaz.setVisible(false);
			    }
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		if(arg0.getSource().equals(label_kvadrat)) {
			try {
				label_prikaz.loadImage("res/images/empty.gif"); // Paint FTW ^^
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		if(arg0.getSource().equals(label_kvadrat)) {
			try {
				label_prikaz.loadImage(slike_duke[izbor.getSelectedIndex()]);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}