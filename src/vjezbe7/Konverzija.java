package vjezbe7;

import java.awt.*;
import java.awt.event.*;

public class Konverzija extends Frame implements ActionListener {

	private Panel konv_panel;
	private TextField temp_celsius;
	private Label c_label, f_label;
	private Button konv_temp;

	// koristimo samo defaultni konstrukt

	public Konverzija() {

		// najprije instanciramo nadklasu
		super("Konverzija iz celzijusa u fahrenheite.");

		// specificiramo dimenzije
		setSize(40, 40);

		konv_panel = new Panel();
		konv_panel.setLayout(new GridLayout(2, 2));

		// dodajmo graficke elemente na converterPanel

		temp_celsius = new TextField();

		c_label = new Label("Celsius");
		konv_temp = new Button("Konverzija");
		f_label = new Label("Fahrenheit");

		// slozimo ih red po red zbog gridlayouta

		konv_panel.add(temp_celsius);
		konv_panel.add(c_label);
		konv_panel.add(konv_temp);
		konv_panel.add(f_label);
		

		// slozimo komponenete
		this.add(konv_panel, BorderLayout.CENTER);
		this.pack();
		this.setVisible(true);

		/*
		 * procesira pritisak na button konv_temp implementirajuci sucelje
		 * java.awt.event. ActionListener
		 */

		konv_temp.addActionListener(this);
		
		// aplikacija zavrsava kada se njezin prozor zatvori
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});

	}

	// implementacija sucelja ActionListener
	public void actionPerformed(ActionEvent event) {
		// Parse degrees Celsius as a double and convert to Fahrenheit.

		if(event.getSource().equals(konv_temp)) {
		/*
		 * uzmi tekst iz temp_celsisus i kovertiraj ga iz celzijusa u fahrenheite
		 * -uocimo da se korsiti staticka metoda Double.parseDouble koja vodi racuna o
		 * tome da li smo upisali broj
		 * 
		 */
		int tempFahr = (int) ((Double.parseDouble(temp_celsius.getText())) * 1.8 + 32);

		f_label.setText(tempFahr + " F");
		}
		else if(event.getActionCommand().equals(0)) {
			System.exit(0);
		}
	}

}
