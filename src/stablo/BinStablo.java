package stablo;

public class BinStablo {
	private Cvor korijen;
	
	public BinStablo(Cvor korijen) {
		this.korijen = korijen;
	}
	
	public Cvor getKorijen() {
		return this.korijen;
	}
}
