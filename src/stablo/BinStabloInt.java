package stablo;

public class BinStabloInt {
	private final BinStablo stablo;

	public BinStabloInt(BinStablo stablo) {
		this.stablo = stablo;
	}

	public BinStablo getStablo() {
		return stablo;
	}

	public int izracunajDubinu() {
		return izracunajDubinu(stablo.getKorijen()) + 1;
	}

	public int brojElemenata() {
		return brojElemenata(stablo.getKorijen());
	}

	public void inorderIspis() {
		inorderIspis(stablo.getKorijen());
	}
	
	public void dodajCvor(IntCvor cvor) {
		dodajCvor(cvor, stablo.getKorijen());
	}
	
	private void dodajCvor(IntCvor cvor, Cvor parent) {
		
		if (cvor.getVrijednost() < ((IntCvor)parent).getVrijednost()) {
			if (parent.getLijevoDijete() == null) {
				parent.setLijevoDijete(cvor);
				return;
			}
			dodajCvor(cvor, parent.getLijevoDijete());
		} else {
			if (parent.getDesnoDijete() == null) {
				parent.setDesnoDijete(cvor);
				return;
			}
			dodajCvor(cvor, parent.getDesnoDijete());
		}
	}

	private int izracunajDubinu(Cvor cvor) {
		if (cvor == null)
			return -1;
		return Math.max(1 + izracunajDubinu(cvor.getLijevoDijete()), 1 + izracunajDubinu(cvor.getDesnoDijete()));
	}

	private int brojElemenata(Cvor cvor) {
		if (cvor == null)
			return 0;
		return 1 + brojElemenata(cvor.getLijevoDijete()) + brojElemenata(cvor.getDesnoDijete());
	}

	private void inorderIspis(Cvor cvor) {
		if (cvor == null)
			return;
		inorderIspis(cvor.getLijevoDijete());
		System.out.print(cvor + " ");
		inorderIspis(cvor.getDesnoDijete());
	}
}
