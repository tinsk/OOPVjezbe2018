package stablo;

/*
 * Cvor binarnog stabla
 */
public interface Cvor {
	//vraca lijevo dijete cvora
    public Cvor getLijevoDijete();    
    
    //postavlja lijevo dijete cvora
    public void setLijevoDijete(Cvor lijevoDijete);
     
    //vraca desno dijete cvora
    public Cvor getDesnoDijete(); 
    
    //postavlja desno dijete cvora
    public void setDesnoDijete(Cvor desnoDijete);
}
