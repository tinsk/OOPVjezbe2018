package stablo;

public class IntCvor implements Cvor {
	int vrijednost;
	Cvor lijevoDijete;
	Cvor desnoDijete;
	
	public IntCvor() {
		vrijednost = 0;
	}
	
	public IntCvor(int vrijednost) {
		this.vrijednost = vrijednost;
	}
	
	public IntCvor(int vrijednost, Cvor lijevoDijete, Cvor desnoDijete) {
		this.vrijednost = vrijednost;
		this.lijevoDijete = lijevoDijete;
		this.desnoDijete = desnoDijete;
	}
	
	public int getVrijednost() {
		return this.vrijednost;
	}

	@Override
	public Cvor getLijevoDijete() {
		return this.lijevoDijete;
	}

	@Override
	public void setLijevoDijete(Cvor lijevoDijete) {
		this.lijevoDijete = lijevoDijete;
	}

	@Override
	public Cvor getDesnoDijete() {
		return this.desnoDijete;
	}

	@Override
	public void setDesnoDijete(Cvor desnoDijete) {
		this.desnoDijete = desnoDijete;
	}
	
	@Override
	public String toString() {
		return Integer.toString(vrijednost);
	}
}
