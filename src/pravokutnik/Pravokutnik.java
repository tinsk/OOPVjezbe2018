
package pravokutnik; //ovo je paket u kojem lezi nasa klasa

/**
 * Klasa modelira pravokutnik u Kartezijevim koordinatama. Pravokutnik je
 * odredjen sa koordinatama svoja dva dijagonalno suprotna vrha (x_1, y_1)
 * (donji ljevi i (x_2, y_2) (gornji desni).
 **/

public class Pravokutnik {

	// klasne varijable opisane gore

	private int x1, y1, x2, y2;

	/**
	 * Iduci konstruktor konstruira objekt iz dane klase koristeci vrhove (x1, y1)
	 * (donji lijevi i (x2, y2) (gornji desni)
	 * @throws NijePravokutnikException 
	 **/

	public Pravokutnik(int x1, int y1, int x2, int y2) throws NijePravokutnikException {
		
		// baciti exception ako zadani brojevi ne cine pravokutnik
		if (x1==x2 || y1==y2) {
			throw new NijePravokutnikException("Ovo nije pravokutnik!");
		}
		
		/**
		 * podsjetimo sa predavanja da se unutar nestatickih metoda klase na klasu
		 * referira sa this
		 **/
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;

	}

	/**
	 * Iduci konstruktor smjestava donji ljevi kut pravokutnika u ishodiste, gornji
	 * desni je onda dan kao (sirina, visina).
	 * @throws NijePravokutnikException 
	 **/

	public Pravokutnik(int sirina, int visina) throws NijePravokutnikException {

		/**
		 * iduca linija koda zove prehodno definirani konstruktor public Pravokutnik
		 * (int x1, int y1, int x2, int y2)
		 **/

		this(0, 0, sirina, visina);
	}

	/**
	 * Podsjetimo da izvedbena okolina programa uvijek dodaje defaultni konstruktor.
	 * Hocemo potpunu kontrolu i ovog konstruktora. Jedna moguca implementacija
	 * @throws NijePravokutnikException 
	 **/

	public Pravokutnik() throws NijePravokutnikException {
		this(0, 0, 1, 1);
	}

	/**
	 * Iduca metoda translatira pravokutnik za vektor (t_x, t_y)
	 */

	public void translatiraj(int t_x, int t_y) {
		x1 += t_x;
		x2 += t_x;
		y1 += t_y;
		y2 += t_y;
	}

	/** Metoda testira je li dana tocka u kvadratu */

	public boolean jeliUnutra(int x, int y) {
		return ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2));
	}

	/**
	 * Metoda odredjuje najmanji objekt iz klase pravokutnik koji je sadrzan u oba.
	 * Ako je presjek prazan skup vraca "degenerirani" pravokutnik simbolizira sa
	 * null. Null je kljucna rijec u Javi koja je oznacava nedefiranu vrijednost za
	 * varijablu koja je tipa reference (polje, objekt, sucelje)
	 * @throws NijePravokutnikException 
	 **/

	public Pravokutnik presjek(Pravokutnik r) throws NijePravokutnikException {// ova metoda ima povratni tip
		// kao i citava klasa.

		/**
		 * Moramo testirati je li presjek dva pravokutnika neprazan ako jest vracamo
		 * null koji simbolizira prazan presjek.
		 */

		/*
		 * Najprije testiramo da mozda sam parvokutnik r nije prazan.
		 */

		if (r == null)
			return null; // vrati nazad prazan presjek

		/**
		 * Ako smo ovdje r nije null i sada konstruiramo pravokutnik r. Utvrdujemo
		 * postoji li presjek. Nacrtajte si sliku.
		 */

		// naprije x-koordinata

		if (x2 < r.x1)
			return null;
		if (r.x2 < x1)
			return null;

		// onda y-koordinata

		if (y2 < r.y1)
			return null;
		if (r.y2 < y1)
			return null;

		// ako smo ovdje onda presjek nije prazan i instanciramo taj
		// pravokutnik

		Pravokutnik prav = new Pravokutnik((this.x1 > r.x1) ? this.x1 : r.x1, (this.y1 > r.y1) ? this.y1 : r.y1,
				(this.x2 < r.x2) ? this.x2 : r.x2, (this.y2 < r.y2) ? this.y2 : r.y2);

		return prav;
	}

	/**
	 * Metoda odredjuje najmanji objekt iz klase pravokutnik koji sadrzi oba.
	 * @throws NijePravokutnikException 
	 **/

	public Pravokutnik unija(Pravokutnik r) throws NijePravokutnikException {

		if (r == null)
			return this; // ako je kojim slucajem r prazan vratimo
		// drugi clan

		return new Pravokutnik((this.x1 < r.x1) ? this.x1 : r.x1, (this.y1 < r.y1) ? this.y1 : r.y1,
				(this.x2 > r.x2) ? this.x2 : r.x2, (this.y2 > r.y2) ? this.y2 : r.y2);
	}

	/**
	 * Podsjetimo da iako ne pise nasa klasa nasljedjuje sve metode iz klase
	 * java.lang.Object. Posebno, mi naslijedjujemo metodu toString() koja
	 * konvertira nas objekt u string. To je korisno npr. ako ga hocemo ispisati sa
	 * metodom poput System.out.println(String c).
	 * 
	 * Procitajte defaultnu implementaciju ove metode. Umjesto nje mi cemo preraditi
	 * tu metodu i ispisati kvadrat na nama razumljiv nacin.
	 **/

	public String toString() {
		return "Pravokutnik ima koordinate donjeg ljevog kuta (" + x1 + "," + y1
				+ "), a koordinate gornjeg desnog kuta su  (" + x2 + "," + y2 + ").";
	}

	// ove metode sluze da se dodje do privatnih varijabli u ovoj klasi

	public int getX1() {
		return x1;
	}

	public int getX2() {
		return x2;
	}

	public int getY1() {
		return y1;
	}

	public int getY2() {
		return y2;
	}
}