package pravokutnik;

import lista.Cvor;

public class PravokutnikCvor extends Pravokutnik implements Cvor {

	private PravokutnikCvor sljedeci;

	public PravokutnikCvor(int sirina, int visina) throws NijePravokutnikException {
		super(sirina, visina);
	}

	public PravokutnikCvor(int x1, int y1, int x2, int y2) throws NijePravokutnikException {
		super(x1, y1, x2, y2);
	}

	public PravokutnikCvor() throws NijePravokutnikException {
		super();
	}

	@Override
	public Cvor getNext() {
		return sljedeci;
	}

	@Override
	public void setNext(Cvor iduci) {
		sljedeci = (PravokutnikCvor) iduci;
	}

	@Override
	public boolean equals(Object obj) {
		if (!Pravokutnik.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		try {
			Pravokutnik p = (Pravokutnik) obj;
			return (getX1() == p.getX1() && getX2() == p.getX2() && getY1() == p.getY1() && getY2() == p.getY2());
		} catch (Exception ex) {
			return false;
		}

	}
}