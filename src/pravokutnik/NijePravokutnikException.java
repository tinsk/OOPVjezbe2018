package pravokutnik;

public class NijePravokutnikException extends Exception {
	public NijePravokutnikException(String message) {
		super(message);
	}
}
