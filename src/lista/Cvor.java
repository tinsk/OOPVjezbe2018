package lista;

/*
definira metode koje dani objekt mora implementirati 
da bi bio u vezanoj listi
*/

public interface Cvor {

	// vraca iduci element u vezanoj listi
	public Cvor getNext();

	// postavlja iduci element u vezanoj listi
	public void setNext(Cvor iduci);
}