package vjezbe8;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

public class TextViewer extends JFrame implements ActionListener {

	private JPanel filePanel;
	private JButton browseButton;
	private JButton clearButton;
	private JTextField locationField;
	private JTextArea text;
	JFileChooser fc;

	public TextViewer() {
		super("TextViewer");
		
		// display text
		text = new JTextArea();
		Border bevelBorder = BorderFactory.createLoweredBevelBorder();
		text.setBorder(bevelBorder);
		text.setEditable(false);
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(text, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.setPreferredSize(new Dimension(400, 300));
		
		// file selection
		filePanel = new JPanel();
		locationField = new JTextField();
		locationField.setPreferredSize(new Dimension(180, 30));
		browseButton = new JButton();
		browseButton.setText("Open");
		browseButton.addActionListener(this);
		fc = new JFileChooser();
		filePanel.add(locationField);
		filePanel.add(browseButton);
		
		// clear all text
		JPanel clearPanel = new JPanel();
		clearButton = new JButton();
		clearButton.setText("Clear all text");
		clearButton.addActionListener(this);
		clearPanel.add(clearButton);
		
		this.getContentPane().add(filePanel, BorderLayout.NORTH);
		this.getContentPane().add(scrollPane, BorderLayout.CENTER);
		this.getContentPane().add(clearPanel, BorderLayout.SOUTH);
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(browseButton)) {
			if (locationField.getText().length() > 1) {
				displayFileContent(Paths.get(locationField.getText()));
			} else {
				openFile();
			}
		} else if(e.getSource().equals(clearButton)) {
			text.setText("");
		}
	}

	private void openFile() {
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			displayFileContent(file.toPath());
			System.out.println("Opening: " + file.getName() + ".\n");
		} else {
			System.out.println("Open command cancelled by user.\n");
		}
	}

	private void displayFileContent(Path path) {
		try {
			byte[] encoded = Files.readAllBytes(path);
			String s = new String(encoded, StandardCharsets.UTF_8);
			text.setText(s);
		} catch (IOException e2) {
			text.setText("Cannot open file: " + e2.getMessage());
			e2.printStackTrace();
		}
	}
}
