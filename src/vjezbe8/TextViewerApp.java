package vjezbe8;

/*
 * Otvara tekstualnu datoteku i prikazuje tekst.
 * Ako nije unesen put do datoteke, otvara se FileChooser dialog.
 */

public class TextViewerApp {

	public static void main(String[] args) {
		new TextViewer();
	}

}
