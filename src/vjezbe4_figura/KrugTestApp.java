package vjezbe4_figura;

public class KrugTestApp {

	public static void main(String[] args) {
		Krug A = new Krug(0,0,5);
		Krug B = new Krug(0,5,1);
		Krug C = new Krug(0,7,1);
		System.out.println("Krug A i B se preklapaju: " + A.preklapaSe(B));
		System.out.println("Krugovi A i C se preklapaju: " + A.preklapaSe(C));
		System.out.println("Srediste kruga B je unutar kruga A: " + A.jeLiUnutra(B.getX(), B.getY()));
		System.out.println("Srediste kruga A je unutar kruga B: " + B.jeLiUnutra(A.getX(), A.getY()));
		System.out.println("Translatiramo krug A za (0,3)");
		A.translatiraj(0, 3);
		System.out.println("Krugovi A i C se preklapaju: " + A.preklapaSe(C));
	}

}
