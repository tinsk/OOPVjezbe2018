package vjezbe4_figura;

public class Krug extends Figura {
	private int x;
	private int y;
	private int r;

	public Krug(int x, int y, int r) {
		this.x = x;
		this.y = y;
		this.r = r;
	}

	public Krug(int r) {
		this(0, 0, r);
	}

	public Krug() {
		this(0, 0, 1);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	// Vraca true ako se krugovi preklapaju
	public boolean preklapaSe(Krug krug) {
		double d = izracunajUdaljenost(this.x, this.y, krug.x, krug.y);

		// ako je udaljenost sredista manja od zbroja radijusa, krugovi se preklapaju
		return (d < (this.r + krug.r));
	}

	@Override
	public void translatiraj(int distanceX, int distanceY) {
		x += distanceX;
		y += distanceY;
	}

	@Override
	public boolean jeLiUnutra(int x, int y) {
		double d = izracunajUdaljenost(this.x, this.y, x, y);
		return d < r;
	}

	// Vraca udaljenost tocki (x1, y1) i (x2, y2)
	private double izracunajUdaljenost(int x1, int y1, int x2, int y2) {
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

}
