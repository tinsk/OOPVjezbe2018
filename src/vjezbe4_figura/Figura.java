package vjezbe4_figura;

public abstract class Figura {
	public abstract void translatiraj(int distanceX, int distanceY);
	public abstract boolean jeLiUnutra(int x, int y);
}
