package vjezbe1;

public class KompleksniBroj {
	private float re;
	private float im;

	public KompleksniBroj() {
		re = 0;
		im = 0;
	}

	public void setRe(float re) {
		this.re = re;
	}

	public void setIm(float im) {
		this.im = im;
	}

	@Override
	public String toString() {
		return String.format("%f", re) + ((im != 0) ? String.format(" + %fi", im) : "");
	}
}
