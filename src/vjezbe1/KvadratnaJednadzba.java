package vjezbe1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Napisite program koji ucitava koeficijente kvadratne jednadzbe te ispisuje
// vrijednost x1 i x2.
public class KvadratnaJednadzba {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Unesi koeficijente kvadratne jednadzbe.");
			System.out.print("a = ");
			float a = scan.nextFloat();
			System.out.print("b = ");
			float b = scan.nextFloat();
			System.out.print("c = ");
			float c = scan.nextFloat();
			List<KompleksniBroj> rjesenja = izracunajKoeficijente(a, b, c);
			for (KompleksniBroj rjesenje : rjesenja) {
				System.out.println(rjesenje);
			}
		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

	public static List<KompleksniBroj> izracunajKoeficijente(float a, float b, float c) {
		List<KompleksniBroj> result = new ArrayList<>();
		KompleksniBroj x1 = new KompleksniBroj();
		KompleksniBroj x2 = new KompleksniBroj();
		float d1 = (float) (Math.pow(b, 2) - 4 * a * c);
		if (d1 < 0) {
			x1.setRe(-b / (2 * a));
			x1.setIm((float) (Math.sqrt(-d1) / (2 * a)));
			x2.setRe(-b / (2 * a));
			x2.setIm((float) (-Math.sqrt(-d1) / (2 * a)));
		} else {
			x1.setRe((-b + (float) Math.sqrt(d1)) / (2 * a));
			x2.setRe((-b - (float) Math.sqrt(d1)) / (2 * a));
		}
		result.add(x1);
		result.add(x2);
		return result;
	}
}
