package vjezbe1;

import java.util.Scanner;

// Napisite program koji ucitava koeficijente kvadratne jednadzbe te ispisuje
// vrijednost x1 i x2.
public class Polinom {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Unesi stupanj polinoma: ");
			int n = scan.nextInt();
			System.out.println("Unesi koeficijente:");
			float[] koeficijenti = new float[n];
			int i = 0;
			while (n-- > 0) {
				System.out.print("Koeficijent uz x^" + n + " = ");
				koeficijenti[i++] = scan.nextFloat();
			}
			System.out.print("Unesi x: ");
			float x = scan.nextFloat();
			double rezultat = izracunajPolinom(koeficijenti, x);
			System.out.println(String.format("Vrijednost je: %f", rezultat));
		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

	public static double izracunajPolinom(float[] koeficijenti, float x) {
		double rezultat = 0;
		float k;
		int potencija;
		for (int i = 0; i < koeficijenti.length; i++) {
			k = koeficijenti[i];
			potencija = koeficijenti.length - i - 1;
			rezultat += Math.pow(x, potencija) * k;
		}
		return rezultat;
	}
}
