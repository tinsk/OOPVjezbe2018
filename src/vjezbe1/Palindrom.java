package vjezbe1;

import java.util.Scanner;

// Ucitava string s tipkovnice i ispisuje je li palindrom
// Racunaju se samo znakovi engleske abecede
public class Palindrom {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Unesi string: ");
			String input = scan.nextLine();
			input = removeNonAlplabetCharacters(input);
			String reverse = reverseString(input);
			System.out.println("Stringovi " + String.format("\"%s\" i \"%s\" ", input, reverse)
					+ (input.equals(reverse) ? "su" : "nisu") + " jednaki");
		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

	public static String removeNonAlplabetCharacters(String input) {
		StringBuilder result = new StringBuilder();
		char c;
		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			if (Character.isLetter(c)) {
				result.append(c);
			}
		}
		return result.toString();
	}

	public static String reverseString(String input) {
		StringBuilder result = new StringBuilder();
		for (int i = input.length() - 1; i >= 0; i--) {
			result.append(input.charAt(i));
		}
		return result.toString();
	}

}
