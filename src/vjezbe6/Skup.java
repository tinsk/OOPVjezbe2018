package vjezbe6;

// Skup elemenata proizvoljnog tipa E
// Uzeli smo genericki tip E jer u zadatku nije definirano kakve objekte skup treba sadrzavati
// https://docs.oracle.com/javase/tutorial/java/generics/types.html
// Nasljedjujedmo Iterable da bismo mogli iterirati po elementima skupa
public interface Skup<E> extends Iterable<E> {
	// Vraca istinu ako je skup podskup skupa s
	boolean jePodskup(Skup<E> s) throws PrazanSkupException;
	
	// Vraca istinu ako je skup jednak skupu s
	boolean jeJednak(Skup<E> s) throws PrazanSkupException;
	
	// Vraca skup koji predstavlja presjek sa skupom s
	Skup<E> presjek(Skup<E> s) throws PrazanSkupException;
	
	// Vraca skup koji predstavlja uniju sa skupom s
	Skup<E> unija(Skup<E> s) throws PrazanSkupException;
	
	// Dodaje element u skup
	// Vraca istinu ako element nije vec postojao u skupu
	boolean dodaj(E element);
	
	// Uklanja element iz skupa
	// Vraca istinu ako je element postojao u skupu
	boolean izbrisi(E element);
	
	// Vraca istinu ako skup sadrzi element
	boolean sadrzi(E element);
	
	// Vraca broj elemenata skupa
	int getBrojElemenata();
	
	// Vraca istinu ako je skup prazan
	boolean jePrazan();
}
