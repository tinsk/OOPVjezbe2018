package vjezbe6;

public class PrazanSkupException extends Exception {
	public PrazanSkupException(String message) {
		super(message);
	}
	
	public PrazanSkupException() {
		super("Skup je prazan.");
	}
}
