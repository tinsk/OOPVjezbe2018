package vjezbe6;

/*
Napisite sucelje koje realizira matematicku apstrakciju skupa. Napisite i
klasu koja radi s tim suceljem i implementira osnovne operacije sa
skupovima: podskup, jednakost, presjek i unija. Napisite i try-catch blok za
provjeru ako je jedan od skupova u operaciji prazan skup te ispisite
odgovarajucu poruku.
 */
public class SkupTestApp {
	public static void main(String[] args) {
		// Kreiramo dva skupa integera i testiramo opeacije na skupovima

		// Dodajemo u skup A elemente 1, 3 i 5
		Skup<Integer> A = new MojSkup<>();
		boolean dodan;
		dodan = A.dodaj(1);
		System.out.println(dodan);
		dodan = A.dodaj(3);
		System.out.println(dodan);
		dodan = A.dodaj(5);
		System.out.println(dodan);
		// pokusamo ponovo dodati element 3
		dodan = A.dodaj(3);
		System.out.println(dodan); 

		System.out.println("A = " + A);

		// Dodajemo u skup B elemente 4, 5, 6, 7, 8
		Skup<Integer> B = new MojSkup<>();
		for (int i = 4; i <= 8; i++) {
			dodan = B.dodaj(i);
			System.out.println(dodan);
		}

		System.out.println("B = " + B);

		try {
			System.out.println("Unija A i B = " + A.unija(B));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}

		try {
			System.out.println("Presjek A i B = " + A.presjek(B));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}

		try {
			System.out.println("A je podskup od B: " + A.jePodskup(B));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}

		try {
			System.out.println("A je naskup od B: " + ((MojSkup<Integer>) A).jeNadskup(B));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}

		try {
			System.out.println("A i B su jednaki: " + A.jeJednak(B));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}

		System.out.println("A i B su jednaki (pomocu equals metode): " + A.equals(B));

		try {
			System.out.println("B je podskup unije skupova A i B: " + B.jePodskup(A.unija(B)));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}

		// Dodajemo u skup C elemente 4, 5, 6, 7, 8
		Skup<Integer> C = new MojSkup<>();
		for (int i = 4; i <= 8; i++) {
			C.dodaj(i);
		}
		System.out.println("C = " + C);
		System.out.println("B i C su jednaki: " + B.equals(C));

		// Kreiramo prazan skup D
		Skup<Integer> D = new MojSkup<>();
		System.out.println("D = " + D);
		try {
			System.out.println("Unija A i D = " + A.unija(D));
		} catch (PrazanSkupException e) {
			e.printStackTrace();
		}
	}
}
