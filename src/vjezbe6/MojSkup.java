package vjezbe6;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// Skup elemenata proizvoljnog tipa E
public class MojSkup<E> implements Skup<E> {

	// Skup (po definiciji) ne moze imati dva jednaka elementa
	// Koristimo Map za cuvanje elemenata skupa zato sto Map vec ima svojstvo da
	// kljuc mora biti jedinstven
	private final Map<E, Object> elementi;

	// Vrijednost za internu mapu elementi
	// Kljucevi predstavljaju elemente, a vrijednosti su uvijek PRISTUAN
	private static final Object PRISUTAN = new Object();

	public MojSkup() {
		elementi = new HashMap<>();
	}

	@Override
	// vraca istinu ako je ovaj skup podskup skupa s
	public boolean jePodskup(Skup<E> s) throws PrazanSkupException {
		// zelimo baciti iznimku ako je ovaj skup ili skup s prazan (jer to trazi
		// zadatak)
		if (this.jePrazan() || s.jePrazan()) {
			throw new PrazanSkupException();
		}

		for (E element : this) {
			if (!s.sadrzi(element)) {
				return false;
			}
		}
		return true;
	}

	// vraca istinu ako je ovaj skup nadskup skupa s
	public boolean jeNadskup(Skup<E> s) throws PrazanSkupException {
		// zelimo baciti iznimku ako je ovaj skup ili skup s prazan
		if (this.jePrazan() || s.jePrazan()) {
			throw new PrazanSkupException();
		}

		for (E element : s) {
			if (!this.sadrzi(element)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean jeJednak(Skup<E> s) throws PrazanSkupException {
		// zelimo baciti iznimku ako je ovaj skup ili skup s prazan (jer to trazi
		// zadatak)
		if (this.jePrazan() || s.jePrazan()) {
			throw new PrazanSkupException();
		}

		if (this.getBrojElemenata() != s.getBrojElemenata()) {
			return false;
		}
		for (E element : this) {
			if (!s.sadrzi(element)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Skup<E> presjek(Skup<E> s) throws PrazanSkupException {
		// zelimo baciti iznimku ako je ovaj skup ili skup s prazan (jer to trazi
		// zadatak)
		if (this.jePrazan() || s.jePrazan()) {
			throw new PrazanSkupException();
		}

		// kreiramo novi skup koji predstavlja presjek
		Skup<E> rezultat = new MojSkup<E>();

		// dodamo u novi skup sve elemente koji se nalaze i u ovom skupu i u skupu s
		for (E element : this) {
			if (s.sadrzi(element)) {
				rezultat.dodaj(element);
			}
		}

		return rezultat;
	}

	@Override
	public Skup<E> unija(Skup<E> s) throws PrazanSkupException {
		// zelimo baciti iznimku ako je ovaj skup ili skup s prazan (jer to trazi
		// zadatak)
		if (this.jePrazan() || s.jePrazan()) {
			throw new PrazanSkupException();
		}

		// kreiramo novi skup koji predstavlja uniju
		Skup<E> rezultat = new MojSkup<E>();

		// dodamo u novi skup sve elemente iz ovog skupa
		for (E element : this) {
			rezultat.dodaj(element);
		}

		// dodamo u novi skup sve elemente iz skupa s
		for (E element : s) {
			rezultat.dodaj(element);
		}

		return rezultat;
	}

	@Override
	public boolean dodaj(E element) {
		return elementi.put(element, PRISUTAN) == null;
	}

	@Override
	public boolean izbrisi(E element) {
		return elementi.remove(element) == PRISUTAN;
	}

	@Override
	public boolean sadrzi(E element) {
		return elementi.containsKey(element);
	}

	@Override
	public int getBrojElemenata() {
		return elementi.keySet().size();
	}

	@Override
	public boolean jePrazan() {
		return getBrojElemenata() == 0;
	}

	// Koristimo iterator iz interne mape za iteriranje elemenata skupa
	@Override
	public Iterator<E> iterator() {
		return elementi.keySet().iterator();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{");
		for (E element : this) {
			sb.append(element);
			sb.append(", ");
		}
		if (sb.lastIndexOf(", ") != -1) {
			sb.delete(sb.lastIndexOf(", "), sb.length());
		}
		sb.append("}");
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (!MojSkup.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		try {
			return this.jeJednak((MojSkup<E>) obj);
		} catch (Exception e) {
			return false;
		}
	}
}
