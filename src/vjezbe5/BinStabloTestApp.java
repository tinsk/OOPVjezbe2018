package vjezbe5;

import stablo.BinStablo;
import stablo.BinStabloInt;
import stablo.Cvor;
import stablo.IntCvor;

public class BinStabloTestApp {
	
	public static void main(String[] args) {
		Cvor cvor2 = new IntCvor(2);

		//kreiramo stablo
		BinStabloInt stablo = new BinStabloInt(new BinStablo(cvor2));
		
		for(int i=0; i<11; i++) {
			stablo.dodajCvor(new IntCvor(i));
		}
		
		//ispisemo stablo
		System.out.print("Inorder ispis: ");
		stablo.inorderIspis();
		System.out.println();
		System.out.println("Broj cvorova: " + stablo.brojElemenata());
		System.out.println("Dubina: " + stablo.izracunajDubinu());
	}
}
