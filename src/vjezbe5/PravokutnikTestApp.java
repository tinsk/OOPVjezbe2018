package vjezbe5;

import lista.Cvor;
import lista.VezanaLista;
import pravokutnik.NijePravokutnikException;
import pravokutnik.Pravokutnik;
import pravokutnik.PravokutnikCvor;

public class PravokutnikTestApp {

	public static void main(String[] args) {
		PravokutnikCvor p1 = null, p2 = null, p3 = null, p4 = null, p5;
		try {
			p1 = new PravokutnikCvor(2, 3);
			p2 = new PravokutnikCvor(3, 4);
			p3 = new PravokutnikCvor(1, 4);
			p4 = new PravokutnikCvor(1, 4);
			// p5 = new PravokutnikCvor(0, 0); // NijePravokutnikException!
		} catch (NijePravokutnikException ex) {
			ex.printStackTrace();
		}

		System.out.println("p3 equals p4: " + (p3.equals(p4)));
		System.out.println("p3 equals p2: " + (p3.equals(p2)));
		

		// dodamo cvorove u listu
		VezanaLista lista = new VezanaLista();
		lista.insertAtHead(p1);
		lista.insertAtHead(p2);
		lista.insertAtHead(p3);

		// ispisemo listu
		Cvor c = lista.getHead();
		while (c != null) {
			System.out.println((PravokutnikCvor) c);
			c = c.getNext();
		}
	}
}
