package vjezbe3;

import java.util.Random;
import java.util.Scanner;

/*
 * Implementirajte klasu koja enkapsulira zbrajanje, mnozenje i
 * transponiranje matrica proizvoljnog ranga. Napisite testnu klasu.
 */

public class MatricaTestApp {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Unesi rang: ");
			int broj = scan.nextInt();
			KvadratnaMatrica A = kreirajMatricu(broj);
			System.out.println("Matrica A: " + A);
			System.out.println("Transponirana matrica: " + A.transponiraj());
			KvadratnaMatrica B = kreirajMatricu(broj);
			System.out.println("Matrica B: " + B);
			System.out.println("Rezultat zbrajanja s matricom B: " + A.zbroji(B));
			System.out.println("Rezultat mnozenja matricom B: " + A.pomnozi(B));

		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}

	public static KvadratnaMatrica kreirajMatricu(int rang) {
		Random rnd = new Random();
		KvadratnaMatrica B = new KvadratnaMatrica(rang);
		for (int i = 0; i < rang; i++) {
			for (int j = 0; j < rang; j++) {
				B.setMatrixField(i, j, rnd.nextInt(10));
			}
		}
		return B;
	}
}
