package vjezbe3;

/* Zadatak
* Napi??ite klasu koja definira djelatnika u radnoj organizaciji specificiraju??i njegovo
* ime i prezime, adresu, rang (0 ako je djlatnik, 1 ako je voditelj), visinu pla??e, te
* odjel u kojem radi. Klasa mora sadr??avati prera??enu metodu toString() koja
* strukturirano ispisuje djelatnika. 
* 
* Klasa djelatnik je implementirana tako da se vrijednosti mogu zadati samo u konstruktoru
* prilikom kreiranja instance, i moguce ih je procitati jedino iz toString() metode.
*/

public class Djelatnik {
	private String ime;
	private String prezime;
	private String adresa;
	private int rang;
	private double visinaPlace;
	private String odjel;
	
	public Djelatnik(String ime, String prezime, String adresa, int rang, double visinaPlace, String odjel) {
		this.ime = ime;
		this.prezime = prezime;
		this.adresa = adresa;
		if (rang < 0 || rang > 1) {
			System.out.println("Rang mora biti 0 ili jedan. Kako unos ne odgovara "
					+ "dozvoljenim vrijednostima, postavlja se automatski na 0.");
			rang = 0;
		}
		this.rang = rang;
		this.visinaPlace = visinaPlace;
		this.odjel = odjel;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\nPodaci o djelatniku\n");
		sb.append(String.format("Ime: %s\n", ime));
		sb.append(String.format("Prezime: %s\n", prezime));
		sb.append(String.format("Adresa: %s\n", adresa));
		sb.append(String.format("Rang: %d\n", rang));
		sb.append(String.format("Visina place: %.2f\n", visinaPlace));
		sb.append(String.format("Odjel: %s\n", odjel));
		return sb.toString();
	}
}
