package vjezbe3;

import java.text.DecimalFormat;

/*
 * Implementirajte klasu koja enkapsulira zbrajanje, mnozenje i
 * transponiranje matrica proizvoljnog ranga.
 */

public class Matrica {
	private final int rows;
	private final int cols;
	private final float[][] matrix;

	public Matrica(int brojRedaka, int brojStupaca) {
		rows = brojRedaka;
		cols = brojStupaca;
		matrix = new float[rows][cols];
	}

	// Vraca transponiranu matricu
	public Matrica transponiraj() {
		Matrica m = new Matrica(cols, rows);
		for (int i = 0; i < cols; i++)
			for (int j = 0; j < rows; j++)
				m.matrix[i][j] = this.matrix[j][i];

		return m;
	}

	// Postojecoj matrici dodaje novu matricu i vraca rezultat zbrajanja.
	// Vraca null ako matrice nisu jednakog ranga.
	public Matrica zbroji(Matrica matrica) {
		if (matrica.rows != rows || matrica.cols != cols)
			return null;

		Matrica rezultat = new Matrica(rows, cols);
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				rezultat.matrix[i][j] = this.matrix[i][j] + matrica.matrix[i][j];

		return rezultat;
	}

	// Postojecu matricu pomnozi matricom mnozitelj i vraca rezultat mnozenja.
	// Vraca null ako broj redaka matrice mnozitelj ne odgovara broj stupaca
	// postojece matrice.
	public Matrica pomnozi(Matrica mnozitelj) {
		if (mnozitelj.rows != cols)
			return null;

		Matrica rezultat = new Matrica(rows, cols);
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				for (int k = 0; k < cols; k++)
					rezultat.matrix[i][j] += this.matrix[i][k] * mnozitelj.matrix[k][j];

		return rezultat;
	}

	// Zadaje vrijednost polja [i,j] matrice
	public void setMatrixField(int i, int j, float value) {
		if (i >= rows || j >= cols) {
			System.out.println("Greska: Polje je izvan ranga matrice.");
			return;
		}
		matrix[i][j] = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("\n");
		DecimalFormat format = new DecimalFormat("##.##");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sb.append(format.format(matrix[i][j]) + "\t");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
