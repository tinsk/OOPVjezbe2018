package vjezbe3;

import java.util.Scanner;

/* Zadatak:
 * Napi??ite i test program koji sadr??i polje sa
 * djelatnicima i koji ih lijepo ispisuje.
 */

public class DjelatnikTestApp {
	
	private static Djelatnik[] djelatnici;
	
	public static void main(String[] argumenti) {
		Scanner scan = new Scanner(System.in);
		short n = 0;
		
		System.out.print("Broj djelatnika (0 za automatsko generiranje): ");
		
		try {
			n = scan.nextShort();
			scan.nextLine();
			if (n > 0 && n < Short.MAX_VALUE) {
				unesiPodatke(n, scan);
			} else {
				generirajPodatke();
			}
		} catch (Exception ex) {
			System.out.println("Pogresan unos. " + ex.getMessage());
		} finally {
			scan.close();
		}
		
		ispisiPodatke();
	}
	
	public static void unesiPodatke(short n, Scanner s) {
		djelatnici = new Djelatnik[n];
		String ime, prezime, adresa, odjel;
		int rang;
		double visinaPlace;
		try {
		for (int i=0; i<n; i++) {
			
			System.out.printf("Djelatnik %d:\n",i);
			System.out.print("Ime: ");
			ime = s.nextLine();
			System.out.print("Prezime: ");
			prezime = s.nextLine();
			System.out.print("Adresa: ");
			adresa = s.nextLine();
			System.out.print("Odjel: ");
			odjel = s.nextLine();
			System.out.print("Rang: ");
			rang = s.nextInt();
			System.out.print("Visina place: ");
			visinaPlace = s.nextDouble();
			Djelatnik d = new Djelatnik(ime, prezime, adresa, rang, visinaPlace, odjel);
			djelatnici[i] = d;
		}
		}
		catch (Exception ex) {
			System.out.println("Pogresan unos. " + ex.getMessage());
		} 
	}
	
	public static void generirajPodatke() {
		djelatnici = new Djelatnik[10];
		for (int i=1; i<=10; i++) {
			djelatnici[i] = new Djelatnik(
					"Ime" + i,
					"Prezime" + i,
					"Adresa" + i,
					i%2,
					6000 + i*100,
					"Odjel"+i);
		}
	}
	
	public static void ispisiPodatke() {
		for(Djelatnik d : djelatnici) {
			System.out.println(d);
		}
	}
}
