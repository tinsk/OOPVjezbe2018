package ponavljanje_quicksort;

import java.util.Random;

public abstract class SortableIntegerArray {
	private int[] intArray;
	private int size;
	
	public SortableIntegerArray(int size) {
		this.size = size;
		intArray = new int[size];
	}
	
	public int[] getIntArray() {
		return intArray;
	}
	
	public void generateRandomArray() {
		Random rnd = new Random();
		for (int i = 0; i < intArray.length; i++) {
			intArray[i] = rnd.nextInt(50);
		}
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		for (int i = 0; i < size - 1; i++) {
			sb.append(intArray[i] + ", ");
		}
		if (size > 0) {
			sb.append(intArray[size - 1]);
		}
		sb.append("]");
		return sb.toString();
	}
	
	public abstract void quickSort();
}
