package ponavljanje_quicksort;

/*
 * Napisite program koji ucitava niz integera u polje te ih sortira. Implementirajte
 * Quick sort algoritam. Program treba ispisati izlaz algoritma u svakom koraku.
 */

public class MyArrayTestApp {
	public static void main(String[] args) {
		
		// initialize an array with 5 elements
		SortableIntegerArray myArray = new MyArray(15);
		
		// fill the array with random integers
		myArray.generateRandomArray();
		
		// print the array
		System.out.println(myArray);
		
		// sort the array
		myArray.quickSort();
		
		// print the sorted array
		System.out.println("Sorted array: " + myArray);
	}
}
