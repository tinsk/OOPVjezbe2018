package ponavljanje_quicksort;

public class MyArray extends SortableIntegerArray {
	public MyArray(int size) {
		super(size);
	}

	@Override
	public void quickSort() {
		quickSort(0, getIntArray().length - 1);
	}

	private void quickSort(int lowerIndex, int higherIndex) {
		int[] array = getIntArray();
		int lower = lowerIndex;
		int higher = higherIndex;
		int pivot = array[lowerIndex + (higherIndex - lowerIndex) / 2];
		while (lower <= higher) {
			while (array[lower] < pivot) {
				lower++;
			}
			while (array[higher] > pivot) {
				higher--;
			}
			if (lower <= higher) {
				swap(lower, higher);
				lower++;
				higher--;
			}
		}

		System.out.println(this);

		if (lowerIndex < higher)
			quickSort(lowerIndex, higher);
		if (lower < higherIndex)
			quickSort(lower, higherIndex);
	}

	private void swap(int i, int j) {
		int[] array = getIntArray();
		if (i >= array.length || j >= array.length) {
			System.out.println("Nemoguce zamijeniti brojeve jer su indeksi izvan polja");
			return;
		}
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
