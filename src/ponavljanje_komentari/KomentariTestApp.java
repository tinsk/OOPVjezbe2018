package ponavljanje_komentari;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Napisite program koji ucitava Java program i ispisuje ga na standardni output tako
 * da izbacite sve komentare van. Razmatrajte sve mogucnosti //, /* */ /*
																		*/
public class KomentariTestApp {

	public static void main(String[] args) {

		// ucitamo java program iz datoteke
		String content = readFileContent("res/test.java");
		System.out.println("\nOriginalna datoteka:\n");
		System.out.println(content);
		
		// brisemo komentare
		BrisacKomentara bk = new BrisacKomentara();
		String output = bk.izbrisiKomentare(content);
		
		// ispisemo rezultat
		System.out.println("\nDatoteka bez komentara:\n");
		System.out.println(output);
		
		// spremimo rezultat u datoteku
		saveContentToFile(output, "res/test-output.java");
	}
	
	public static String readFileContent(String path) {
		File file = new File(path);
		char[] buffer = null;

		try(BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
			buffer = new char[(int) file.length()];

			int i = 0;
			int c = bufferedReader.read();

			while (c != -1) {
				buffer[i++] = (char) c;
				c = bufferedReader.read();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return new String(buffer);
	}
	
	public static void saveContentToFile(String content, String path) {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path)))){
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
