package ponavljanje_komentari;

public class BrisacKomentara {
	public String izbrisiKomentare(String content) {
		char[] input = content.toCharArray();
		int len = input.length;
		boolean inString = false, inMultiLineComment = false, inSingleLineComment = false;
		char c;
		int i = 0;

		// we will add all characters outside of comments to the output
		StringBuilder output = new StringBuilder();

		while (i < len) {
			c = input[i];

			// if we run into the beginning of a string, copy characters until the end of
			// the string. we need to ignore comment symbols inside strings
			if (c == '"') {
				output.append(c);
				i++;

				inString = true;
				while (inString && i < len) {
					c = input[i];
					output.append(c);
					// if we found the end of the string, we can exit the loop
					if (c == '"' && i - 1 >= 0 && input[i - 1] != '\\') {
						inString = false;
					}
					i++;
				}
			} else if (c == '/' && i + 1 < len && input[i + 1] == '*') { // if we found the start of a multiline comment
				// do not copy characters until the end of the comment
				inMultiLineComment = true;
				while (inMultiLineComment && i + 1 < len) {
					i++;
					c = input[i];
					if (c == '/' && i - 1 >= 0 && input[i - 1] == '*') {
						inMultiLineComment = false;
						i++;
					}
				}
			} else if (c == '/' && i + 1 < len && input[i + 1] == '/') { // if we found a single line coment
				// do not copy characters until the end of the line
				c = input[++i];
				inSingleLineComment = true;
				while (inSingleLineComment && i < len) {
					if (c == '\n') {
						inSingleLineComment = false;
						i++;
					}
					c = input[i];
					i++;
				}
			} else { // otherwise, copy the character to the output
				output.append(c);
				i++;
			}
		}

		return output.toString();
	}
}
