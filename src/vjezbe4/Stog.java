package vjezbe4;

public class Stog {
	private Object[] objStog;
	private int kapacitet;
	private int zadnjiIndeks;

	public Stog(int kapacitet) {
		this.kapacitet = kapacitet;
		objStog = new Object[kapacitet];
		zadnjiIndeks = -1;
	}

	public boolean isEmpty() {
		return zadnjiIndeks == -1;
	}
	
	public void push(Object c) {
		if (zadnjiIndeks + 1 >= kapacitet) {
			System.out.println("\nStog nema mjesta za dodavanje novog elementa: " + c.toString());
			return;
		}
		zadnjiIndeks++;
		objStog[zadnjiIndeks] = c;
	}
	
	public Object pop() {
		if (isEmpty()) {
			System.out.println("Polje je prazno.");
			return null;
		}
		return objStog[zadnjiIndeks--];
	}
}
