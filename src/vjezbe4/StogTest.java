package vjezbe4;

import java.util.Random;
import java.util.Scanner;

public class StogTest {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Kreiramo stog kapaciteta 10");
			Stog stog = new Stog(10);
			System.out.println("Je li stog prazan? " + stog.isEmpty());
			System.out.print("Dodajemo 5 slucajnih integera u stog: ");
			Random rnd = new Random();
			for (int i=0; i<5; i++) {
				int n = rnd.nextInt(100);
				System.out.print(n + " ");
				stog.push(n);
			}
			System.out.println();
			System.out.println("Je li stog prazan? " + stog.isEmpty());
			System.out.println("Skidamo zadnji element: " + stog.pop());
			System.out.println("Skidamo zadnji element: " + stog.pop());
			System.out.print("Dodajemo 8 slucajnih integera u stog: ");
			for (int i=0; i<8; i++) {
				int n = rnd.nextInt(100);
				System.out.print(n + " ");
				stog.push(n);
			}
			System.out.println("Skidamo zadnji element: " + stog.pop());
		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}
}
