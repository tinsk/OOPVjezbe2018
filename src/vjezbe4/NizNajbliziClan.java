package vjezbe4;

public class NizNajbliziClan extends NizAritmetickaSredina {
	public NizNajbliziClan(int brojClanova, int max) {
		super(brojClanova, max);
	}

	@Override
	public int dohvatiClanNajbliziSredini() {
		int[] niz = getNiz();
		if (niz.length < 1) {
			System.out.println("Greska: Niz nema nijedan clan.");
			return 0;
		}
		float sredina = izracunajAritmetickuSredinu();
		float minUdaljenost = Float.MAX_VALUE;
		int najbliziClan = niz[0];
		float razlika;
		for (int i = 0; i < niz.length; i++) {
			razlika = Math.abs(sredina - niz[i]);
			if (razlika <= minUdaljenost) {
				minUdaljenost = razlika;
				najbliziClan = niz[i];
			}
		}
		return najbliziClan;
	}
}
