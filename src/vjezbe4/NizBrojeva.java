package vjezbe4;

import java.util.Random;

public abstract class NizBrojeva {

	private int brojClanova;
	private int max;
	private int[] niz;

	public NizBrojeva(int brojClanova, int max) {
		this.brojClanova = brojClanova;
		this.max = max;
		generirajNiz();
	}

	protected void generirajNiz() {
		Random rnd = new Random();
		niz = new int[brojClanova];
		for (int i = 0; i < brojClanova; i++) {
			niz[i] = rnd.nextInt(max);
		}
	}

	protected int[] getNiz() {
		return this.niz;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		for (int i = 0; i < brojClanova - 1; i++) {
			sb.append(niz[i] + ", ");
		}
		if (brojClanova > 0) {
			sb.append(niz[brojClanova - 1]);
		}
		sb.append("]");
		return sb.toString();
	}

	public abstract float izracunajAritmetickuSredinu();

	public abstract int dohvatiClanNajbliziSredini();
}
