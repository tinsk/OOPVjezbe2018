package vjezbe4;

public abstract class NizAritmetickaSredina extends NizBrojeva {
	public NizAritmetickaSredina(int brojClanova, int max) {
		super(brojClanova, max);
	}

	@Override
	public float izracunajAritmetickuSredinu() {
		int[] niz = getNiz();
		int suma = 0;
		for (int i = 0; i < niz.length; i++) {
			suma += niz[i];
		}
		return suma / niz.length;
	}

	@Override
	public abstract int dohvatiClanNajbliziSredini();
}
