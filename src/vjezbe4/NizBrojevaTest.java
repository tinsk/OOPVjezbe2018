package vjezbe4;

import java.util.Scanner;

/*
 * Napišite apstraktnu klasu koji napravi niz od n slučajnih prirodnih brojeva
manjih od N. Napišite i klasu koja nasljeđuje apstraktnu klasu te posjeduje
metodu za izračun aritmetičke sredine niza. Napišite i subklasu koja
određuje koji je član niza najbliži aritmetičkoj sredini.
 * 
 */

public class NizBrojevaTest {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			System.out.print("Unesi velicinu niza: ");
			int n = scan.nextInt();
			int max = 100;
			NizNajbliziClan niz = new NizNajbliziClan(n, max);
			System.out.println("Niz: " + niz);
			System.out.println("Aritmeticka sredina: " + niz.izracunajAritmetickuSredinu());
			System.out.println("Clan najblizi sredini: " + niz.dohvatiClanNajbliziSredini());

		} catch (Exception ex) {
			System.out.println("Doslo je do greske. " + ex.getMessage());
		} finally {
			scan.close();
		}
	}
}
